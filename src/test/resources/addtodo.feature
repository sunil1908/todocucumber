Feature: Add Todo
  Scenario: Create todo by POST /todos
    Given ensure rest endpoint of "http://35.184.238.253:8080" is up
    When a POST request to /todos is made
    And the request body is
      """
{
  "targetDate": "2020-03-03",
  "description": "Test",
  "user": "Anil",
  "done": false
}
      """
    Then a 201 response is returned within 2000ms